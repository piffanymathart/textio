package TextIO;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class TextIOTest {

	@Test
	void readWrite() {

		String s1 = "hello\nhello";
		String s2 = "bye\nbye";

		Path path = Paths.get("TextIO\\src\\Test\\TextIO\\test.txt");

		try {
			TextIO.write(path, s1);
			assertEquals(s1, TextIO.read(path));
			TextIO.write(path, s2);
			assertEquals(s2, TextIO.read(path));
		}
		catch(IOException e) {
			assert(false);
		}
	}
}