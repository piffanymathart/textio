package TextIO;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * A helper class for performing text file I/O.
 */
public interface TextIO {

	/**
	 * Reads text files from the data directory to strings.
	 * @param path The file path.
	 * @return The file as a string.
	 * @throws IOException If file reading fails.
	 */
	static String read(Path path) throws IOException {
		return new String(Files.readAllBytes(path));
	}

	/**
	 * Writes strings to text files in the data directory.
	 * @param path The file path.
	 * @param s The string.
	 * @throws IOException If file writing fails.
	 */
	static void write(Path path, String s) throws IOException {
		FileWriter fileWriter = new FileWriter(path.toString(), false);
		fileWriter.write(s);
		fileWriter.flush();
		fileWriter.close();
	}
}
